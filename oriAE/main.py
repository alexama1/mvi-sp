import glob

from keras_preprocessing.image import img_to_array
from matplotlib import pyplot as plt
import numpy as np
from keras.layers import Input, Conv2D, MaxPooling2D, UpSampling2D
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.preprocessing.image import load_img, img_to_array, ImageDataGenerator
from keras.optimizers import RMSprop

LOAD_WEIGHTS = True

IMG_DIM = (200, 200)
INPUT_SHAPE = (200, 200, 3)
ORIGINAL_CATALOGUE_IMAGES_PATH = "/media/skvara/WDC/oriless"
EPOCHS = 10
BATCH_SIZE = 8
TEST_SIZE = 10
WEIGHTS_FILENAME = "ori_autoencoder_generator_{}.h5".format(EPOCHS)


def extract_data(input_path, num_images):
    filenames = glob.glob(input_path + '/' + '*.jpeg')
    filenames = np.random.choice(filenames, size=num_images, replace=False)
    loaded_images = []
    for image in filenames:
        loaded_images.append(img_to_array(load_img(image, target_size=IMG_DIM)))
    return np.array(loaded_images)


def get_generator(path, keep_original=False):
    # Generate data from the images in a folder
    if keep_original:
        data_gen = ImageDataGenerator(data_format='channels_last')
    else:
        data_gen = ImageDataGenerator(rescale=1. / 255, zoom_range=0.3, rotation_range=50, shear_range=0.2,
                                      horizontal_flip=True, fill_mode='nearest', data_format='channels_last')

    return data_gen.flow_from_directory(
        path,
        target_size=IMG_DIM,
        batch_size=BATCH_SIZE,
        class_mode='input'
    )


def encoder(input_img):
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(input_img)
    conv1 = BatchNormalization()(conv1)
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv1)
    conv1 = BatchNormalization()(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
    conv2 = BatchNormalization()(conv2)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2)
    conv2 = BatchNormalization()(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
    conv3 = BatchNormalization()(conv3)
    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv3)
    conv3 = BatchNormalization()(conv3)
    conv4 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv3)
    conv4 = BatchNormalization()(conv4)
    conv4 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv4)
    conv4 = BatchNormalization()(conv4)
    return conv4


def decoder(conv4):
    conv5 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv4)
    conv5 = BatchNormalization()(conv5)
    conv5 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv5)
    conv5 = BatchNormalization()(conv5)
    conv6 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv5)
    conv6 = BatchNormalization()(conv6)
    conv6 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv6)
    conv6 = BatchNormalization()(conv6)
    up1 = UpSampling2D((2, 2))(conv6)
    conv7 = Conv2D(32, (3, 3), activation='relu', padding='same')(up1)
    conv7 = BatchNormalization()(conv7)
    conv7 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv7)
    conv7 = BatchNormalization()(conv7)
    up2 = UpSampling2D((2, 2))(conv7)
    decoded = Conv2D(3, (3, 3), activation='sigmoid', padding='same')(up2)
    return decoded


def create_autoencoder():
    input_img = Input(shape=INPUT_SHAPE)
    _autoencoder = Model(input_img, decoder(encoder(input_img)))
    _autoencoder.compile(loss='mean_absolute_error', optimizer=RMSprop())
    _autoencoder.summary()
    return _autoencoder


def train_model(_autoencoder):
    if LOAD_WEIGHTS:
        print("Loading weights")
        _autoencoder.load_weights(WEIGHTS_FILENAME)
    else:
        print("Training")
        train_generator = get_generator(ORIGINAL_CATALOGUE_IMAGES_PATH)
        validation_generator = get_generator(ORIGINAL_CATALOGUE_IMAGES_PATH, keep_original=True)
        autoencoder_train = _autoencoder.fit(
            train_generator,
            steps_per_epoch=1000 // BATCH_SIZE,  # 1000
            epochs=EPOCHS,
            validation_data=validation_generator,
            validation_steps=1000 // BATCH_SIZE,  # 1000
            verbose=1)

        print("Saving weights")
        _autoencoder.save_weights(WEIGHTS_FILENAME)

        loss = autoencoder_train.history['loss']
        val_loss = autoencoder_train.history['val_loss']
        # accuracy = autoencoder_train.history['acc']
        epochs_range = range(EPOCHS)
        plt.figure()
        plt.plot(epochs_range, loss, 'bo', label='Training loss')
        plt.plot(epochs_range, val_loss, 'b', label='Validation loss')
        # plt.plot(epochs_range, accuracy, 'b', label='Accuracy')
        plt.title('Training and validation loss')
        plt.legend()
        plt.savefig("history.png")


def test_model(_autoencoder):
    bad_data = extract_data("./test/invalid", TEST_SIZE)
    bad_data = bad_data.reshape(TEST_SIZE, 200, 200, 3)
    bad_data = bad_data / np.max(bad_data)

    good_data = extract_data("./test/valid", TEST_SIZE)
    good_data = good_data.reshape(TEST_SIZE, 200, 200, 3)
    good_data = good_data / np.max(good_data)

    # predict
    pred_good = _autoencoder.predict(good_data)
    pred_bad = _autoencoder.predict(bad_data)
    # reconstruction error
    rec1 = np.sum(np.abs(pred_good - good_data) ** 2, axis=(1, 2, 3))
    rec2 = np.sum(np.abs(pred_bad - bad_data) ** 2, axis=(1, 2, 3))
    # histogram
    plt.figure(figsize=(10, 5))
    plt.subplot(2, 1, 1)
    plt.hist(rec1, bins=30, range=(0, np.max(rec2)), color='g')
    plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    plt.ylabel("count of good images")
    plt.subplot(2, 1, 2)
    plt.hist(rec2, bins=30, range=(0, np.max(rec2)), color='r')
    plt.xlabel("reconstruction error")
    plt.ylabel("count of bad images")
    plt.savefig("reconstruction_error.png")
    plt.close()

    # Show predicted images from best to worst
    # sort = np.sort(rec2)
    # for value in sort:
    #     original_index = np.where(rec2 == value)[0]
    #     print(value)
    #     f, axarr = plt.subplots(1, 2)
    #     axarr[0].imshow(bad_data[original_index][0])
    #     axarr[0].set_title("original")
    #
    #     axarr[1].imshow(pred_bad[original_index][0])
    #     axarr[1].set_title("predicted")
    #     plt.show()


def show_predicted_image(_autoencoder, generator):
    # show predicted image
    data = generator.next()
    predicted = _autoencoder.predict(data)
    f, axarr = plt.subplots(1, 2)
    axarr[0].imshow(data[0][0])
    axarr[0].set_title("original")

    axarr[1].imshow(predicted[0])
    axarr[1].set_title("predicted")
    plt.show()


autoencoder = create_autoencoder()
train_model(autoencoder)
test_model(autoencoder)
# show_predicted_image(autoencoder, train_generator)
