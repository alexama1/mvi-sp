from matplotlib import pyplot as plt

from keras import optimizers
from keras.applications.resnet50 import ResNet50
from keras.layers import Dropout, Flatten, Dense
from keras.models import Sequential, Model
from keras.preprocessing.image import ImageDataGenerator

IMG_DIM = (200, 200)
input_shape = (200, 200, 3)
ORIGINAL_CATALOGUE_IMAGES_PATH = "/media/skvara/WDC/oriless"
MAX_IMAGES = 2000
BATCH_SIZE = 8
EPOCHS = 20
WEIGHTS_FILENAME = 'ori_tlearn_img_aug_cnn_restnet50_finetuned_generator_{}.h5'.format(EPOCHS)
LOAD_WEIGHTS = True


def get_generator(path, keep_original=False):
    # Generate data from images in a folder
    if keep_original:
        data_gen = ImageDataGenerator(rescale=1. / 255, data_format='channels_last')
    else:
        data_gen = ImageDataGenerator(rescale=1. / 255, zoom_range=0.3, rotation_range=50, shear_range=0.2,
                                      horizontal_flip=True, fill_mode='nearest', data_format='channels_last')

    return data_gen.flow_from_directory(
        path,
        target_size=IMG_DIM,
        batch_size=BATCH_SIZE,
        class_mode='binary'
    )


resnet = ResNet50(include_top=False, weights='imagenet', input_shape=input_shape)
output = resnet.layers[-1].output
output = Flatten()(output)
resnet = Model(resnet.input, outputs=output)
for layer in resnet.layers:
    layer.trainable = False
resnet.summary()

resnet.trainable = True
trainable_flag = False
for layer in resnet.layers:
    if layer.name in ['conv5_block3_1_conv']:
        trainable_flag = True
    if trainable_flag:
        layer.trainable = True
    else:
        layer.trainable = False
model_finetuned = Sequential()
model_finetuned.add(resnet)
model_finetuned.add(Dense(512, activation='relu', input_dim=input_shape))
model_finetuned.add(Dropout(0.3))
model_finetuned.add(Dense(512, activation='relu'))
model_finetuned.add(Dropout(0.3))
model_finetuned.add(Dense(1, activation='sigmoid'))
model_finetuned.compile(loss='binary_crossentropy',
                        optimizer=optimizers.RMSprop(lr=1e-5),
                        metrics=['accuracy'])
model_finetuned.summary()

if LOAD_WEIGHTS:
    print("Loading weights")
    model_finetuned.load_weights(WEIGHTS_FILENAME)
else:
    print("Training model")

    train_generator = get_generator(ORIGINAL_CATALOGUE_IMAGES_PATH)
    validation_generator = get_generator(ORIGINAL_CATALOGUE_IMAGES_PATH, keep_original=True)
    history_finetuned = model_finetuned.fit(
        train_generator,
        steps_per_epoch=1000 // BATCH_SIZE,
        epochs=EPOCHS,
        validation_data=validation_generator,
        validation_steps=1000 // BATCH_SIZE,
        verbose=1)
    model_finetuned.save(WEIGHTS_FILENAME)

    # loss = history_finetuned.history['loss']
    # # val_loss = history_finetuned.history['val_loss']
    # # accuracy = autoencoder_train.history['acc']
    # epochs_range = range(EPOCHS)
    # plt.figure()
    # plt.plot(epochs_range, loss, 'bo', label='Training loss')
    # # plt.plot(epochs_range, val_loss, 'b', label='Validation loss')
    # # plt.plot(epochs_range, accuracy, 'b', label='Accuracy')
    # plt.title('Training and validation loss')
    # plt.legend()
    # plt.savefig("history.png")

print("Testing")
test_generator = get_generator("./test", keep_original=True)

score = model_finetuned.evaluate(test_generator, verbose=1)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

false_negative_counter = 0
total_counter = 0
dropped_counter = 0
for i in range(0, 30):
    data = test_generator.next()
    score = model_finetuned.predict(data)
    for data_v, label, score_v in zip(data[0], data[1], score):
        total_counter += 1
        if score_v[0] < 0.1:
            dropped_counter += 1
            if label == 1.0:
                false_negative_counter += 1

        # Show original image and print score
        # print("score = ", score_v, "original label = ", label)
        # f, axarr = plt.subplots(1, 1)
        # axarr.imshow(data_v)
        # axarr.set_title("original")
        # plt.show()
print("total tested =", total_counter, "total dropped =", dropped_counter, "wrongly dropped =", false_negative_counter)
