# Filtrovánı́ fotografiı́ katalogu

Naše firma provozuje službu pro rozpoznávání katalogových stránek pro Oriflame Holding AG. Jednotlivé stránky jsou identifikovány pomocí feature-matching algoritmu.


Cílem této semestrální práce je odfiltrovat fotografie, které mají malou pravděpodobnost rozpoznání na základě keypointů z feature-matching algoritmu, tedy neobsahují katalogovou stránku k rozpoznání.


# Obsah repozitáře
Závěrečný report - report.pdf

V rámci práce byly vyzkoušeny dva přístupy:
- Klasické rozpoznání pomocí konvolučních neuronových sítí - složka oriNN
- Filtrace na základě chyby při rekonstrukci pomocí autoenkodéru - složka oriAE

Pro vypořádání se s problémem špatných označení jsem napsal jednoduchý skript v jazyce Python, který se hrubou silou snaží porovnat keypointy z algoritmu SIFT z knihovny OpenCV - složka oriSift.


K otestování je přiložená malá podmnožina dat. Vytvořené váhy jsou k dispozici ke stažení [na mém Google drivu](https://drive.google.com/drive/folders/1nN_xVzVnucU4q6eiJHvRo837YonSDI61?usp=sharing).


