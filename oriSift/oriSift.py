import cv2
import os
import pickle
import shutil

PROCESSED_DIRECTORY = "/media/skvara/WDC/UbuntuOld/Downloads/production.europe.vision.synetech.cz/PROCESSED/"
MATCHED_DIRECTORY = "/media/skvara/WDC/UbuntuOld/Downloads/production.europe.vision.synetech.cz/MATCHED/"
ORIGINAL_CATALOGUE_IMAGES_PATH = "/media/skvara/WDC/oriless/valid/"
sift = cv2.SIFT_create()
bf = cv2.BFMatcher()
original_map = {}


def match(kp1, des1, kp2, des2):
    matches = bf.knnMatch(des1, des2, k=2)
    good = []
    for m, n in matches:
        if m.distance < 0.8 * n.distance:
            good.append(m)

    if len(kp1) >= len(kp2):
        keypoints_number = len(kp1)
    else:
        keypoints_number = len(kp2)
    return len(good) / keypoints_number


def copy_file_to_valid(directory, matched, original):
    _matched = "matched" + matched
    _original = matched + original
    shutil.copy2(directory + matched, "./images/valid/" + _matched)
    shutil.copy2(ORIGINAL_CATALOGUE_IMAGES_PATH + original, "./images/valid/" + _original)


def copy_file_to_invalid(directory, not_matched, original):
    _not_matched = "notmatched" + not_matched
    _original = not_matched + original
    shutil.copy2(directory + not_matched, "./images/invalid/" + _not_matched)
    shutil.copy2(ORIGINAL_CATALOGUE_IMAGES_PATH + original, "./images/invalid/" + _original)


def detect_original(filename):
    # cache results for original catalogue images
    if not original_map.get(filename):
        img = cv2.imread(ORIGINAL_CATALOGUE_IMAGES_PATH + filename, 0)
        kp2, des2 = sift.detectAndCompute(img, None)
        original_map[filename] = [kp2, des2]
    return original_map.get(filename)


def save_keypoints():
    # I was not able to test this, as it ate too much memory
    for key in original_map.keys():
        value = original_map.get(key)
        points = value[0]
        new_kps = []
        for point in points:
            new_kps.append((point.pt, point.size, point.angle, point.response, point.octave,
                            point.class_id))
        original_map[key] = [new_kps, value[1]]

    with open('data.p', 'wb') as fp:
        pickle.dump(original_map, fp, protocol=pickle.HIGHEST_PROTOCOL)


def load_keypoints():
    with open('data.p', 'rb') as fp:
        original_map = pickle.load(fp)
    for key in original_map.keys():
        value = original_map.get(key)
        points = value[0]
        new_kps = []
        for point in points:
            new_kps.append(cv2.KeyPoint(x=point[0][0], y=point[0][1], _size=point[1], _angle=point[2],
                                        _response=point[3], _octave=point[4], _class_id=point[5]))
        original_map[key] = [new_kps, value[1]]
    return original_map


flag = False
for filename in os.listdir(MATCHED_DIRECTORY)[0:8000]:
    if filename == "1702afb0-7ece-4a63-80e0-12b11335c8c1.jpeg":
        flag = True
        continue
    if not flag:
        continue
    counter = 0
    if '.json' in filename or '~' in filename or os.path.getsize(MATCHED_DIRECTORY + filename) == 0:
        continue
    print(filename)
    img = cv2.imread(MATCHED_DIRECTORY + filename, 0)
    kp1, des1 = sift.detectAndCompute(img, None)

    best = ''
    best_score = 0.
    for filename2 in os.listdir(ORIGINAL_CATALOGUE_IMAGES_PATH):
        counter += 1
        if counter % 1000 == 0:
            print(counter)
        if os.path.getsize(ORIGINAL_CATALOGUE_IMAGES_PATH + filename2) == 0:
            continue
        kp2, des2 = detect_original(filename2)
        score = match(kp1, des1, kp2, des2)
        if score > best_score:
            print("found new best score", score, "file", filename2)
            best_score = score
            best = filename2
            if best_score >= 0.3:
                if filename2 == "656a203d407f4c8da90e74c339e727cb.jpeg" \
                        or filename2 == "7c25648c636b4cee82d4ca875766fe23.jpeg":
                    # filter out test images
                    break
                copy_file_to_valid(MATCHED_DIRECTORY, filename, filename2)
                break
    print("the best match for", filename, "is", best, "with", best_score)

for filename in os.listdir(PROCESSED_DIRECTORY):
    counter = 0
    if '.json' in filename or '~' in filename or os.path.getsize(PROCESSED_DIRECTORY + filename) == 0:
        continue
    print(filename)
    img = cv2.imread(PROCESSED_DIRECTORY + filename, 0)
    kp1, des1 = sift.detectAndCompute(img, None)

    best = ''
    best_score = 0.
    for filename2 in os.listdir(ORIGINAL_CATALOGUE_IMAGES_PATH):
        counter += 1
        if counter % 1000 == 0:
            print(counter)
        if os.path.getsize(ORIGINAL_CATALOGUE_IMAGES_PATH + filename2) == 0:
            continue
        kp2, des2 = detect_original(filename2)
        score = match(kp1, des1, kp2, des2)
        if score > best_score:
            print("found new best score", score, "file", filename2)
            best_score = score
            best = filename2
            if best_score >= 0.3:
                if filename2 == "656a203d407f4c8da90e74c339e727cb.jpeg" \
                        or filename2 == "7c25648c636b4cee82d4ca875766fe23.jpeg":
                    # filter out test images
                    break
                copy_file_to_valid(PROCESSED_DIRECTORY, filename, filename2)
                break

    if best_score < 0.3:
        copy_file_to_invalid(PROCESSED_DIRECTORY, filename, best)
    print("the best match for", filename, "is", best, "with", best_score)
